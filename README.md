# skillbox-test-task
###### forked from [here](https://gitlab.com/sergeymyratkin/flatris)

1 A [.gitlab-ci.yml](https://gitlab.com/zenmike/flatris/-/blob/master/.gitlab-ci.yml) file is used to integrate with gitlab ci.
  - at first it was used without any docker registry stuff: https://gitlab.com/zenmike/flatris/-/blob/a9bb363435160697cc9067cc0c777951e620de82/.gitlab-ci.yml
  - after adding [integration with docker registry](https://gitlab.com/zenmike/flatris/-/commit/3b26ad84438a31ac2a094a6aca11617024483df8), [added](https://gitlab.com/zenmike/flatris/-/commit/f3ff0d1c7e6ea12204bc50d9ff1de293152a7a13) test stage to pipeline. job was [sucesful](https://gitlab.com/zenmike/flatris/-/jobs/580172885).

2 To create the k8s cluster [flatris-k8s-deployment.yml](https://gitlab.com/zenmike/flatris/-/blob/master/flatris-k8s-deployment.yml) file was added. 
  - at first section of the file (Service) we creating a loadblancer, so it will use 80 port to accept requests from web and 3000 port to move request to the app itself.
  - at the second section of file (Deployment) we creating a deployment resource from our [docker image](registry.gitlab.com/zenmike/flatris:latest) on port 3000.
  - [demonstartion](https://asciinema.org/a/cJMwGVtVA5M0zATO6EVhaeBco) of Deployment creation. [screenshot](https://i.imgur.com/ByLVuvu.png) of working app.


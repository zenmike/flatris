FROM node

RUN mkdir /opt/app/
WORKDIR /opt/app

COPY package.json /opt/app
RUN yarn install

COPY . /opt/app/

#RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000

